FROM node:8.4.0-slim
WORKDIR /app
COPY . .
RUN    npm install --save express \
    && npm install --save redis

ENTRYPOINT [ "node", "server.js" ]

EXPOSE 3000
